#!/usr/bin/env python
# coding: utf-8

# In[70]:


import numpy as np
import pandas as pd
import copy
import seaborn as sns
import matplotlib.pyplot as plt
import string

# (optional) Defining empty grid

#grid=np.zeros([8,8],dtype=int)
#grid=grid.tolist()

#uploading grid from the input file

grid = pd.read_csv("Input.csv", sep = ";", skiprows=1)

#converting to matrix form

grid = np.asmatrix(grid)

# (optional) Defining empty grid (another way to do it, also the initial 1st queen may be put on the grid if needed)

#grid123=[[0,0,0,0,0,0,0,0],
#      [0,0,0,0,0,0,0,0],
#      [0,0,0,0,0,0,0,0],
#      [0,0,0,0,0,0,0,0],
#      [0,0,0,0,0,0,0,0],
#      [0,0,0,0,0,0,0,0],
#      [0,0,0,0,0,0,0,0],
#      [0,0,0,0,0,0,0,0]]

grid = grid.tolist()
def possible(grid, y, x):  # Check if it possible to place a queen into y,x position

     l = len(grid)  # the length of the grid
     for i in range(l):  # check for queens on row y
         if grid[y][i] == 1:  # if exist return false
             return False
     for i in range(l):  # check for queens on column x
         if grid[i][x] == 1:  # if exists return 0
             return False

     for i in range(l):  # loop through all rows
         for j in range(l):  # and columns
             if grid[i][j] == 1:  # if there is a queen
                 if abs(i - y) == abs(j - x):  # and if there is another on a diagonal
                     return False  # return false
     return True  # if every check clears, return true


def solve(grid):
     l = len(grid)  # the length of the grid

     for y in range(l):  # for every row
         for x in range(l):  # for every column
             if grid[y][x] == 0:  # we can place if there is no queen in given position
                 if possible(grid, y, x):  # if empty, check if we can place a queen
                     grid[y][x] = 1  # if we can, then place it
                     solve(grid)  # pass grid for recursive solution
                     # if we end up here, means we searched through all children branches
                     if sum(sum(a) for a in grid) == l:  # if there are 8 queens
                         return grid  # success
                     grid[y][x] = 0  # remove the previously placed queen

     return grid


Solution = solve(grid) #get the solution
print("Solution:\n")
print(np.matrix(Solution),"\n") #Print the solution
print("Solution on a chess board:")

def plot(grid):  # Plot the solution on a chessboard using seaborn

     l = len(grid)
     Ly = list(range(1, l + 1))[::-1]
     ly = [str(i) for i in Ly]
     Lx = list(string.ascii_uppercase)
     lx = Lx[:l]

     plt.close('all')
     sns.set(font_scale=2)
     plt.figure(figsize=(10, 10))
     ax = plt.gca()
     ax.set_aspect(1)
     sns.heatmap(Solution, linewidths=.8, cbar=False, linecolor='blue',
                 cmap='Reds', center=0.4, xticklabels=lx, yticklabels=ly)
        
plot(grid)
plt.savefig('output.pdf')