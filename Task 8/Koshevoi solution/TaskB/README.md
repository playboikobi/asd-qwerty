# ASD Task 8 solution
1) 8 Queens problem solution using Python3 and Main/Subroutine with Shared Data method
2) Key Word In Context problem solution using Python3 and Pipes-and-filters method
# 8Q problem Guide:
1) Check that numpy, pandas, seaborn and matplotlib libraries are installed
2) Put all the files from the 8Q_problem folder into one folder
3) Run the Main.py file via Jupyter Notebook (recommended, in order to get visualization), via other development environments or via a command line

The result of 8Q problem solution on a chess board (screenshot from Jupyter Notebook):
![image](https://user-images.githubusercontent.com/43605718/200078311-cf1ffc3e-8adb-4035-9d26-22c769a4f223.png)

# Additional note:
The first "Queen" can be put into any cell in the Input.csv file
Output contains the solution image showing the positions of 8 queens
