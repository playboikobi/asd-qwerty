def cut_to_sentence(text, keyword, keywordindex):
    safe = ["Ms", "Mr", "Fr", "Hr", "Dipl", "B", "M", "Sc", "Dr", "Prof",
            "Mo", "Mon", "Di", "Tu", "Tue", "Tues", "Mi", "Wed", "Do", "Th",
            "Thu", "Thur", "Thurs", "Fr", "Fri", "Sa", "Sat", "So", "Sun",
            "0", "1", "2", "3", "4", "5", "6", "7", "8", "9",
            "str"]

    # Find beginning
    rfind_results = []
    end_ = keywordindex
    # Special Case "."
    while True:
        rfind_ = text.rfind(". ", 0, end_)
        if not rfind_ == -1:
            no_safe = False
            for i, s in enumerate(safe):
                if text[0:rfind_][::-1].find(s[::-1]) == 0:
                    end_ = rfind_ - len(s)
                    break
                if i == len(safe) - 1:
                    no_safe = True
            if no_safe is True:
                break
        else:
            break
    rfind_results.append(rfind_)

    rfind_results.append(max([text.rfind(sentence_ending, 0, keywordindex)
                              for sentence_ending in ["! ", "? "]]))

    rfind_result = max(rfind_results)
    if rfind_result == -1:
        start = 0
    else:
        start = rfind_result + 2

    # Find ending
    find_results = []
    start_ = keywordindex + len(keyword)
    # Special Case "."
    while True:
        find_ = text.find(". ", start_)
        if not find_ == -1:
            no_safe = False
            for i, s in enumerate(safe):
                if text[0:find_][::-1].find(s[::-1]) == 0:
                    start_ = find_ + len(s)
                    break
                if i == len(safe) - 1:
                    no_safe = True
            if no_safe is True:
                break
        else:
            break
    find_results.append(find_)

    find_results.extend([text.find(sentence_ending, keywordindex + len(keyword))
                         for sentence_ending in ["! ", "? "]])
    find_results_bigger_neg_1 = [i for i in find_results if i >= 0]
    if not find_results_bigger_neg_1:
        end = len(text)
    else:
        end = min(find_results_bigger_neg_1) + 1

    return list(range(start, end)), text[start:end]


def find_nth_occurrence(text, searchstr, nth=1, startindex=0):
    """
    Finds the index of the nth occurence of a searchstr in the text starting
    from the a given startindex.
    """
    start = text.find(searchstr, startindex)

    if start == -1:
        return len(text) - 1

    for i in range(nth - 1):
        find_index = text.find(searchstr, start + len(searchstr))
        if find_index == -1:
            return len(text) - 1
        else:
            start = find_index

    return start


def rfind_nth_occurrence(text, searchstr, nth=1, endindex=None):
    """
    Finds the index of the nth occurence of a searchstr in the text going
    backwards from a given endindex.
    """
    if endindex is None:
        endindex = len(text)

    end = text.rfind(searchstr, 0, endindex)

    if end == -1:
        return 0

    for i in range(nth - 1):
        rfind_index = text.rfind(searchstr, 0, end)
        if rfind_index == -1:
            return 0
        else:
            end = rfind_index

    return end


def keywords_in_context(text, keywords, max_words=5, sep="...", cut_sentences=True):
    indices_lst = []
    for k in keywords:
        start = text.find(k)
        while not start == -1:
            indices_lst.append((k, start))
            start = text.find(k, start + len(k))

    result_indices = set()
    for index_tpl in indices_lst:
        keyword, index = index_tpl
        start = rfind_nth_occurrence(text, " ", nth=max_words + 1, endindex=index)
        if not start == 0:
            start += 1  # +1 to Remove the first " "
        end = find_nth_occurrence(text, " ", nth=max_words + 1, startindex=index + len(keyword))
        if end == len(text) - 1:
            end += 1
        indices_of_text = set(range(start, end))
        if cut_sentences:
            sentence_indices, _ = cut_to_sentence(text, keyword, index)
            indices_of_text.intersection_update(set(sentence_indices))
        for i in indices_of_text:
            result_indices.add(i)

    result_indices = list(result_indices)
    result_indices.sort()

    result = ""
    i_before = -1
    for _i, i in enumerate(result_indices):
        if not (i - 1) == i_before:
            result += " " + sep + " " + text[i]
            i_before = i
        else:
            result += text[i]
            i_before = i

        # If the last word is not the end of the text add the sperator.
        if _i == len(result_indices) - 1:
            if not i == len(text) - 1:
                result += " " + sep

    return result


def find_and_replace(text, find_str, replacement_str):
    """ Find and replace a find_str with a replacement_str in text. """
    start = text.find(find_str)
    offset = 0
    while start != -1:
        # update the index compatible to the whole text
        start = start + offset

        # replace (cut the original word out and insert the replacement)
        text = text[:start] + replacement_str + text[start + len(find_str):]

        offset = start + len(replacement_str)
        start = text[offset:].find(find_str)

    return text


if __name__ == "__main__":

    TEXT = ('Outside, in suburban Bloomfield Hills, a dozen miles north of Detroit, it was still dark.The GM president - a spare, fast-moving, normally even-tempered man - had another cause for ill temper besides the electric blanket. It was Emerson Vale. A few minutes ago, through the radio turned on softly beside his bed, the GM chief had heard a news broadcast which included the hated, astringent, familiar voice of the auto industrys arch critic,Yesterday, at a Washington press conference, Emerson Vale had blasted anew his favorite targets - General Motors, Ford, and Chrysler. The press wire services, probably due to a lack of hard news from other sources, had obviously given Vales attack the full treatment.The big three of the auto industry, Emerson Vale charged, were guilty of "greed, criminal conspiracy, and self-serving abuse of public trust." The conspiracy was their continuing failure to develop alternatives to gasoline-powered automobiles - namely electric and steam vehicles - which, Vale asserted, "are available now."The accusation was not new. However, Vale - a skilled hand at public relations and with the press - had injected enough recent material to make his statement newsworthy.The president of the worlds largest corporation, who had a Ph.D. in engineering, fixed the blanket control, in the same way that he enjoyed doing other jobs around the house when time permitted. Then he showered, shaved, dressed for the office, and joined Coralie at breakfast.A copy of the Detroit Free Press was on the dining-room table. As he saw Emerson Vales name and face prominently on the front page, he swept the newspaper angrily to the floor."Well," Coralie said. "I hope that made you feel better." She put a cholesterol-watchers breakfast in front of him - the white of an egg on dry toast, with sliced tomatoes and cottage cheese, The GM presidents wife always made breakfast herself and had it with him, no matter how early his departure. Seating herself opposite, she retrieved the Free Press and opened it.')

    KEYWORDS = ["GM", "Coralie", "breakfast", "president"]

    result_text = keywords_in_context(TEXT, KEYWORDS)
    # Highlight Keywords
    for k in KEYWORDS:
        result_text = find_and_replace(result_text, k, "\x1b[34m" + k + "\x1b[0m")

    print(result_text)